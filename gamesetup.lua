
-- Make this usable in local functions
local ModPath = _G.ModPath

_G.ProcedualMapGen = _G.ProcedualMapGen or {}
_G.ProcedualMapGen.ModPath = _G.ModPath
_G.ProcedualMapGen.ModInstance = _G.ModInstance

ProcedualMapGen.PrefabLoader = blt.vm.dofile(ModPath .. "geom/PrefabLoader.lua")
ProcedualMapGen.C = blt.vm.dofile(ModPath .. "map/Constants.lua")

-- Probably should rename, since this was copied from BeardLib Editor, let's keep
-- the name the same for now. If it gets confusing later we can change it.
ProcedualMapGen.ExportUtils = blt.vm.dofile(ModPath .. "dep_scan/ExportUtils.lua")

local mission_manager = blt.vm.dofile(ModPath .. "sys/CustomMissionScript.lua")
local custom_world_def = blt.vm.dofile(ModPath .. "sys/WorldDef.lua")
ProcedualMapGen.LuaCallbackElement = blt.vm.dofile(ModPath .. "sys/LuaCallbackElement.lua")

-- Lines: 609 to 653
-- coped, either use me or delete me!
local old_init_game = GameSetup.init_game
function GameSetup:init_game(...)
	if Global.level_data.level ~= "virtual_mapgen_levelname" or Application:editor() then
		return old_init_game(self, ...)
	end

	-- Add some ugly hacks in required for bits and pieces, that we don't want permanently enabled
	blt.vm.dofile(ModPath .. "sys/UglyEngineHacks.lua")

	-- Here on it's all custom stuff, only used for our custom level

        local gsm = Setup.init_game(self)

	local engine_package = PackageManager:package("engine-package")

	engine_package:unload_all_temp()
	managers.mission:set_mission_filter(managers.job:current_mission_filter() or {})

	local level = Global.level_data.level
	local mission = Global.level_data.mission
	local world_setting = Global.level_data.world_setting
	local level_class_name = Global.level_data.level_class_name
	local level_class = level_class_name and rawget(_G, level_class_name)

	if level_class then
		script_data.level_script = level_class:new()
	end

	custom_world_def:init()

	-- local level_path = "levels/" .. tostring(level)
	-- local t = {
	-- 	file_type = "world",
	-- 	file_path = level_path .. "/world",
	-- 	world_setting = world_setting
	-- }
	-- assert(WorldHolder:new(t):create_world("world", "all", Vector3()), "Cant load the level!")

	managers.worlddefinition:init_done()

	-- local mission_params = {
	-- 	stage_name = "stage1",
	-- 	file_path = level_path .. "/mission",
	-- 	activate_mission = mission
	-- }
	-- managers.mission:parse(mission_params)

	blt.xpcall(function()
		mission_manager:exec_mission_stuff(custom_world_def)
	end, function(msg)
		log("ERROR failed to exec mission script stuff: " .. tostring(msg))
		log(debug.traceback())
	end)

	return gsm
end

local old_update = GameSetup.update
function GameSetup:update(t, dt)
	old_update(self, t, dt)

	if Global.level_data.level ~= "virtual_mapgen_levelname" then
		return
	end

	blt.xpcall(function()
		-- Required for delays in scripts to work
		mission_manager:update(dt)
	end, function(msg)
		log("ERROR failed update mission manager: " .. tostring(msg))
		log(debug.traceback())
	end)
end

