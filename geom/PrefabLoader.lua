local PrefabLoader = class()

function PrefabLoader:init()
	self.checked_units = {}
end

function PrefabLoader:spawn(path, pos, rot, origin)
	path = ProcedualMapGen.ModPath .. path
	local f = assert(io.open(path, "rb"), "Could not open " .. path)
	--local data = ScriptSerializer:from_binary(f:read("*a"))
	local data = ScriptSerializer:from_custom_xml(f:read("*a"))
	f:close()

	origin = origin or Vector3(0, 0, 0)

	local units = {}

	for _, tbl in ipairs(data) do
		assert(tbl.type == "unit")
		local unit = self:_hit_unit(tbl.unit_data, pos, rot, origin)

		if tbl.unit_data.name_id then
			units[tbl.unit_data.name_id] = unit
		end
	end

	return units
end

function PrefabLoader:include_unit(name)
	if self.checked_units[name] then
		return
	end
	self.checked_units[name] = true

	-- Ignore our custom assets
	if name:find("payday2_tiles") then
		return
	end

	local config = ProcedualMapGen.ExportUtils:GetDependencies("unit", name)
	assert(config ~= false)

	local to_load = {}

	for _, item in ipairs(config) do
		local ext = item._meta
		local name = item.path

		blt.wren_io.invoke(ProcedualMapGen.ModInstance.id, "DB_IO", nil, "add_bundle_fallback", ext, name)

		if item.load or item._meta == "unit" or item._meta == "effect" then
			-- We can't load it just yet, since we need all the dependencies available first
			table.insert(to_load, item)
		end
	end

	for _, item in ipairs(to_load) do
		local ext = item._meta
		local name = item.path
		if not managers.dyn_resource:has_resource(ext:id(), name:id(), managers.dyn_resource.DYN_RESOURCES_PACKAGE) then
			-- Is logging this useful?
			-- log("[PrefabLoader] Dyn-res loading " .. name .. "." .. ext)
			managers.dyn_resource:load(ext:id(), name:id(), managers.dyn_resource.DYN_RESOURCES_PACKAGE)
		end
	end
end

function PrefabLoader:_hit_unit(data, pos, rot, origin)
	self:include_unit(data.name)
	return World:spawn_unit(Idstring(data.name), (data.position - origin):rotate_with(rot) + pos, data.rotation * rot)
end

return PrefabLoader:new()
