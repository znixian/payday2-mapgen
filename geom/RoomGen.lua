local RoomGen = class()

-- TODO move all instances of this into a single location
local cell_size = ProcedualMapGen.C.cell_size

-- Data for how walls are placed for each direction they can go in
local walls_data = {
	{ -- front X (0,0->w,0)
		dir = Vector3(1,0),
		rot = Rotation(math.UP, 270),
		start = Vector3(0,0), -- multiplied by the room size and used as a starting offset
		count = "x", -- which side field to use
		out_normal = Vector3(0, -1), -- pointing away from the room
		door_trim = true, -- Of two adjacent rooms with a non-mission doorway, who generates the stuff that covers both sides?
	},
	{ -- front Y (0,0->0,d)
		dir = Vector3(0,1),
		rot = Rotation(math.UP, 180),
		start = Vector3(0,0), -- multiplied by the room size and used as a starting offset
		count = "y", -- which side field to use
		offset = Vector3(0, 1),
		out_normal = Vector3(-1, 0), -- pointing away from the room
		door_trim = true,
	},
	{ -- rear X (0,d->w,d)
		dir = Vector3(1,0),
		rot = Rotation(math.UP, 90),
		start = Vector3(0,1), -- multiplied by the room size and used as a starting offset
		count = "x", -- which side field to use
		offset = Vector3(1, 0),
		out_normal = Vector3(0, 1), -- pointing away from the room
		door_trim = false,
	},
	{ -- rear Y (w,0->w,d)
		dir = Vector3(0,1),
		rot = Rotation(math.UP, 0),
		start = Vector3(1,0), -- multiplied by the room size and used as a starting offset
		count = "y", -- which side field to use
		out_normal = Vector3(1, 0), -- pointing away from the room
		door_trim = false,
	},
}

local wall_to_inface = Rotation(math.UP, -90)

function RoomGen:spawn_unit(name, pos, rot)
	-- Make sure this unit is loaded, and if not, load it
	ProcedualMapGen.PrefabLoader:include_unit(name)

	-- Actually spawn the unit
	return World:spawn_unit(name:id(), pos, rot)
end

function RoomGen:generate(room, world_def)
	-- local size = math.min(room.w, room.d)

	local floor_type = ({
		"a", "b", "c", "d",
	})[math.random(4)]

	local dbg_i = 0

	local pos = Vector3()
	local rot = Rotation()

	local room_x = room.x * cell_size / 100
	local room_y = room.y * cell_size / 100
	local room_w = room.w * cell_size / 100
	local room_d = room.d * cell_size / 100

	local xo = 0
	while xo < room_w do
		local remaining_x = room_w - xo
		local size_x = remaining_x >= 4 and 4 or 1

		local yo = 0
		while yo < room_d do
			local remaining_y = room_d - yo
			local size_y = remaining_y >= 4 and 4 or 1

			local tile_size = math.min(size_x, size_y)

			-- Spawn tiles of size size_x,size_y
			for xxo = 0,size_x-1,tile_size do
				mvector3.set_static(pos, (xxo + xo + room_x) * 100, (yo + room_y) * 100, room.z --[[+dbg_i]])
				local name = tile_size == 4 and "4x4" or "1x1_a"
				dbg_i = dbg_i + 1
				self:spawn_unit("units/payday2_tiles/architecture/red/floor_" .. floor_type .. "/floor_" .. name, pos, rot)
				-- log("spawn at " .. tostring(xxo + xo) .. "x" .. tostring(yo) .. " (" .. tostring(size_x) .. "x" .. tostring(size_y) .. ")")
			end

			yo = yo + tile_size
		end

		xo = xo + size_x
	end

	local doors = {}

	for _, door in ipairs(room._doors) do
		doors[door.x] = doors[door.x] or {}
		doors[door.x][door.y] = door
	end

	-- Spawn the walls
	local decoration_positions = {}
	self:_spawn_wall(room, walls_data[1], doors, world_def, decoration_positions)
	self:_spawn_wall(room, walls_data[2], doors, world_def, decoration_positions)
	self:_spawn_wall(room, walls_data[3], doors, world_def, decoration_positions)
	self:_spawn_wall(room, walls_data[4], doors, world_def, decoration_positions)

	-- Spawn the props
	if not room._spec then return end
	local data = room._spec._data

	-- Wall/side of room props
	for _, unit_name in ipairs(data.props or {}) do
		if #decoration_positions == 0 then
			break
		end

		local pos_data = table.remove(decoration_positions, math.random(#decoration_positions))
		local pos = pos_data[1]
		local rot = pos_data[2]
		local spec = ProcedualMapGen.Spec:get_unit(unit_name)
		if spec._data.wall_mount then
			mvector3.set_z(pos, pos.z + 125)
		end
		spec:pd2_spawn(pos, rot)
	end

	-- Floor props
	local function rand_axis(room_units, footprint)
		local margin = 200 + footprint / 2
		local move_range = room_units * cell_size - margin * 2
		return margin + math.random() * move_range
	end

	for _, unit_name in ipairs(data.floor_props or {}) do
		mvector3.set_static(pos, room_x * 100, room_y * 100, room.z)
		local unit = ProcedualMapGen.Spec:get_unit(unit_name)
		if unit:is_corner() then
			mvector3.set_x(pos, pos.x + ProcedualMapGen.C.wall_displacement)
			mvector3.set_y(pos, pos.y + ProcedualMapGen.C.wall_displacement)
		else
			mvector3.set_x(pos, pos.x + rand_axis(room.w, unit._footprint.x))
			mvector3.set_y(pos, pos.y + rand_axis(room.d, unit._footprint.y))
		end
		unit:pd2_spawn(pos, Rotation())
	end
end

function RoomGen:generate_scripts(room, mission_manager)
	-- Custom room generators may override this
end

local spawn_wall_pos = Vector3()
local spawn_wall_dir = Vector3()

function RoomGen:_spawn_wall(room, data, doors, world_def, decoration_positions)
	local x = room.x + data.start.x * room.w + (data.offset and data.offset.x or 0)
	local y = room.y + data.start.y * room.d + (data.offset and data.offset.y or 0)
	mvector3.set_static(spawn_wall_pos, x * cell_size, y * cell_size, room.z)

	mvector3.set(spawn_wall_dir, data.dir)
	mvector3.multiply(spawn_wall_dir, cell_size)

	local x = (room.w-1) * data.start.x
	local y = (room.d-1) * data.start.y

	local inwards_dir = data.rot * wall_to_inface
	local wall_centre_offset = spawn_wall_dir / 2 - (data.offset or Vector3()) * cell_size

	-- Find the padding to apply to the wall, to not make it wafer-thin
	local wall_thickness_pad = Vector3(0, -ProcedualMapGen.C.wall_displacement, 0)
	mvector3.rotate_with(wall_thickness_pad, inwards_dir)

	local count = room[data.count == "x" and "w" or "d"]
	for i=1, count do
		local door_info = doors[x] and doors[x][y]
		local door = door_info and door_info.other

		-- Don't count the door if the target room is facing away
		-- This can occur on the corner of our room, if we have two neighbours touching that corner. Without this, two doors
		-- would be added.
		if door and not door:contains(room.x + x + data.out_normal.x, room.y + y + data.out_normal.y) then
			door = nil
		end

		if door then
			-- Note: also consider using lux_int_wall1_4m_a_doubledoor
			self:spawn_unit("units/payday2/architecture/lux/lux_int_wall1_2m_door_a", spawn_wall_pos + wall_thickness_pad, data.rot)

			-- Since the door is a ½ cell wide and there's a ¼ cell gap on either side, we need to shift the door across by ¼ a cell
			local door_pos = Vector3(0, cell_size * 0.25, 0)

			-- And that needs to be matched to the wall's direction, giving us our final offset
			mvector3.rotate_with(door_pos, data.rot)

			-- Turn the offset into an absolute position, and spawn the door
			mvector3.add(door_pos, spawn_wall_pos)

			-- Embed the door back into the wall so it lines up with the other side
			mvector3.subtract(door_pos, wall_thickness_pad)

			-- Spawn in mission doors - see Room:add_doorway_to for more information
			local mission_data = door_info.mission_data
			if mission_data ~= nil then
				local unit = self:spawn_unit("units/payday2/equipment/gen_interactable_door_keycard/gen_interactable_door_keycard", door_pos, data.rot)

				-- Register it with the WorldDef, will later be used to enable/disable pathfinding when opened
				world_def:register_door(unit, room)

				-- Note: another 'type' of door would be units/equipment/c4_charge/c4_plantable
				-- With units/payday2/architecture/hcm/hcm_int_wall_explosion
				-- With units/payday2/architecture/hcm/hcm_int_wall_explosion_debris
				-- Use units/equipment/c4_charge/c4_plantable
			elseif data.door_trim then
				-- Spawn the door cap - this only needs to be done once, and data.door_trim is set for only one of a pair of opposite wall datas
				-- This is just a bit of trim that makes the interface look acceptable
				local unit = self:spawn_unit("units/payday2/architecture/lux/lux_int_wall_door_cap_a", door_pos, data.rot * Rotation(math.UP, 180))
			end
		else
			self:spawn_unit("units/payday2/architecture/lux/lux_int_wall1_2m_a", spawn_wall_pos + wall_thickness_pad, data.rot)
			if decoration_positions and i ~= 1 and i ~= count then
				table.insert(decoration_positions, {spawn_wall_pos + wall_centre_offset, inwards_dir})
			end
		end

		mvector3.add(spawn_wall_pos, spawn_wall_dir)

		if data.count == "x" then
			x = x + 1
		elseif data.count == "y" then
			y = y + 1
		else
			error("No known count field: " .. data.count)
		end
	end
end

return RoomGen
