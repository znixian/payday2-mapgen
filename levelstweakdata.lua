Hooks:PostHook(LevelsTweakData, "init", "AddMapgenLevelTweakdata", function(self)
	self.mapgen = {
		name_id = "heist_mapgen_hl",
		briefing_id = "heist_mapgen_briefing",
		briefing_dialog = "Play_pln_framing_stage1_brief",
		world_name = "virtual_mapgen_levelname", -- "narratives/e_framing_frame/stage_1",
		intro_event = "Play_pln_framing_stage1_intro_a",
		outro_event = {
			"Play_pln_framing_stage1_end_a",
			"Play_pln_framing_stage1_end_b"
		},
		music = "heist",
		package = {"packages/narr_framing_1"},
		cube = "cube_apply_heist_bank",
		ghost_bonus = 0.075,
		max_bags = 13,
		ai_group_type = america,
		load_screen = "guis/dlcs/pic/textures/loading/job_framingframe_01",


		player_sequence = "spawn_prop_raincoat",
	}

	table.insert(self._level_index, "mapgen")
end)

