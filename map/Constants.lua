-- Note this file may either by loaded by scripts in the map directory with require() and our
-- custom environment, or directly from gamesetup.lua - so don't depend on either.

return {
    -- Defines the translation from a mapgen cell to the world coordinate space
    -- Notably a doorway in the mapgen is one unit wide, so this fits nicely
    cell_size = 200, -- 1 mapgen unit = 2 metres

    -- The distance in towards a room the walls are shifted by. This is what determines
    -- how thick the walls are (this is per-side, so double this). It's a global since it's
    -- needed when placing objects against a wall to avoid them clipping through.
    -- This value isn't ideal - security doors look ugly on the 'secure' side, with a thin gap to the void
    wall_displacement = 12.5,
}
