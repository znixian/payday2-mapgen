local MissionGen = require("MissionGen")
local ZoneGenerator = require("ZoneGenerator")
local IntraZoneLayout = require("IntraZoneLayout")
local ZoneFuser = require("ZoneFuser")

--[[
-- The Generator class encapsulates all parts of the generation cycle, and returns data directly usable to spawn a level.
--]]

local Generator = class()

function Generator:generate()
	local mission = MissionGen:new():generate()
	local zones = ZoneGenerator:new():generate(mission)

	local i = 1
	for _, zone in ipairs(zones) do
		-- TODO what size to use?
		local intragen = IntraZoneLayout:new(25, 25)
		zone.rooms = intragen:generate()

		intragen:debug_log_rooms(zone.rooms)

		zone.id = i
		i = i + 1
	end

	local fuser = ZoneFuser:new()
	local fused_rooms = fuser:fuse(zones[1])

	dprint("result: ")
	local testing = IntraZoneLayout:new(fused_rooms._maxx, fused_rooms._maxy)
	testing:debug_log_rooms(fused_rooms._rooms)

	return fused_rooms
end

return Generator
