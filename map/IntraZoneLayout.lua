local Room = require("Room")
local RoomList = require("RoomList")
local Spec = require("spec/Spec")

-- TODO remove in favour of RoomList:any_overlap
local function is_valid(placed, r)
	for _, room in ipairs(placed) do
		if room:overlaps(r) and room ~= r then
			return false
		end
	end

	return true
end

local Generator = class()

function Generator:init(size_x, size_y)
	self._size_x = size_x
	self._size_y = size_y
end

function Generator:generate()
	local roomlist = {}

	-- Place in some extra rooms
	dprint("laying out")
	local fails = 0
	while true do
		if not self:_add_room(roomlist) then
			fails = fails + 1
			if fails > 5 then
				break
			end
		else
			fails = 0
		end
	end
	dprint("done")

	-- if prev then
	-- 	local hw = Hallway:new(1, {
	-- 		{x = prev.x, y = prev.y + prev.d},
	-- 		{x = r.x, y = r.y - 1},
	-- 	})
	-- 	table.insert(roomlist, hw)
	-- end

	-- prev = r

	local centrex = math.floor(self._size_x / 2)
	local centrey = math.floor(self._size_y / 2)

	local actioned
	repeat
		actioned = false
		for _, room in ipairs(roomlist) do
			actioned = actioned or self:_migrate_towards(roomlist, room, centrex, centrey)
		end
	until not actioned

	-- Create a temporary RoomList, which we can use to recalculate neighbours and stuff
	-- The variable naming here makes this a little confusing
	local roomlist_obj = RoomList:new(roomlist)

	-- Find each room's neighbours
	roomlist_obj:recalculate_all_neighbours()

	-- Add in doorways
	-- TODO make this a bit more selective
	-- For now, just put a room for every neighbour
	for _, room in ipairs(roomlist) do
		for _, np in ipairs(room._neighbours) do
			if np.first == room then
				room:add_doorway_to(np.second)
			end
		end
	end

	return roomlist
end

function Generator:debug_log_rooms(roomlist)
	if not dprint_enabled then return end

	local grid = {}
	local function mark(x,y, sym)
		grid[x] = grid[x] or {}
		grid[x][y] = sym
	end

	for _, room in ipairs(roomlist) do
		for xx=0, room.w-1 do
			for yy=0, room.d-1 do
				local px = room.x + xx
				local py = room.y + yy
				local hori = xx == 0 or xx == room.w-1
				local vert = yy == 0 or yy == room.d-1
				local has_door = false

				if hori or vert then
					for _, door in ipairs(room._doors) do
						if door.x == xx and door.y == yy then
							has_door = door
						end
					end
				end

				if has_door then
					local md = has_door.mission_data
					mark(px, py, md and md.map_char or "^")
				elseif hori and vert then
					mark(px, py, "*")
				elseif hori then
					mark(px, py, "|")
				elseif vert then
					mark(px, py, "-")
				else
					mark(px, py, room.marker or "x")
				end
			end
		end
	end

	for y=1, self._size_y do
		local s = tostring(y)
		while s:len() < 4 do
			s = s .. " "
		end
		for x=1, self._size_x do
			s = s .. (grid[x] and grid[x][y] or ".")
		end
		log(s)
	end
end

function Generator:_migrate_towards(roomlist, room, tgt_x, tgt_y)
	local orig_x = room.x
	local orig_y = room.y

	local dir_x = orig_x < tgt_x and 1 or orig_x > tgt_x and -1 or 0
	local dir_y = orig_y < tgt_y and 1 or orig_y > tgt_y and -1 or 0

	if dir_x ~= 0 then
		room.x = orig_x + dir_x
		room.y = orig_y
		if is_valid(roomlist, room) then return true end
	end

	if dir_y ~= 0 then
		room.x = orig_x
		room.y = orig_y + dir_y
		if is_valid(roomlist, room) then return true end
	end

	room.x = orig_x
	room.y = orig_y
	return false
end

function Generator:_add_room(roomlist)
	local room = Spec:random_room():build_room()

	local attempts = 0

	repeat
		self:_randomise_room(room)

		attempts = attempts + 1
		if attempts > 1000 then
			return false
		end

	until is_valid(roomlist, room)

	table.insert(roomlist, room)
	return true
end

function Generator:_randomise_room(r)
	r.x = math.random(self._size_x - r.w)
	r.y = math.random(self._size_y - r.d)
end

function Generator:_layout(rooms)
	local placed = {}

	dprint("start placing")

	for _, room in ipairs(rooms) do
		local attempts = 0
		repeat
			self:_randomise_room(room)

			attempts = attempts + 1
			if attempts > 1000 then
				return false
			end

		until is_valid(placed, room)

		table.insert(placed, room)
	end

	dprint("done placing")
	return true
end

return Generator

