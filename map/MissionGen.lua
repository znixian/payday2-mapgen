if not class then
	class = require("nopd2/class")
end

local Templates = require("objectives")
local Task = require("Task")

local MissionGen = class()

local by_cat = {}
for name, obj in pairs(Templates.instances) do
	local cat = by_cat[obj.type] or {}
	by_cat[obj.type] = cat

	table.insert(cat, name)
end

local key_chance = 0.5

function MissionGen:generate()
	local start = Task:new("entry")
	local tail = start

	for i=1,3 do
		if math.random() < key_chance then
			tail:add_sub("key")
			tail = tail:add_sub("key_door")
		else
			tail = tail:add_sub("delay")
		end
	end

	tail = tail:add_sub("loot")

	local list = {}
	local function add_to_list(room)
		table.insert(list, room)

		for _, con in ipairs(room:subs()) do
			add_to_list(con)
		end
	end
	add_to_list(start)

	-- table.insert(list, pick_template("loot", list))
	-- 
	-- for _, i in ipairs(list) do
	-- 	print("template", i.name)
	-- end


	if dprint_enabled then
		local function pr(pre, room)
			dprint(pre .. "room objective: " .. room:name())

			for _, con in ipairs(room:subs()) do
				pr(pre .. " ", con)
			end
		end
		pr("", start)
	end

	return {
		start = start,
		rooms = list,
	}
end

return MissionGen

