local RoomList = require("RoomList")

local Room = class()

function Room:init()
	-- The points a door can be made to. Note these are the relative positions of the adjacent
	-- cell in the other room. For example, consider the room's origin 0,0. A position -1,0 would let a
	-- door attach to the -x direction to the 0,0 cell.
	-- Nil means there's no restriction. This is mostly for pre-fabricated rooms (those designed
	-- by hand or pinched from PD2's existing maps).
	self.door_connection_points = nil

	-- The letter used to show the room in the debug maps, or the default if nil
	self.marker = nil

	self._connections = {}
	self._parent = nil

	self._tasks = {}
	self._neighbours = {}
	self._doors = {}

	self._pd2_generator = nil
end

function Room:overlaps(other)
	return self:intersects(other.x, other.y, other.w, other.d)
end

function Room:intersects(x, y, w, d)
	-- Is this room fully to the left or right of the other
	if self.x + self.w <= x or x + w <= self.x then return false end

	-- Is this room fully above or below the other?
	if self.y + self.d <= y or y + d <= self.y then return false end

	-- TODO z

	-- In this case we must be overlapping
	return true
end

function Room:contains(x, y)
	-- In this case we must be overlapping
	return (self.x <= x and x < self.x + self.w) and (self.y <= y and y < self.y + self.d)
end

function Room:add_task(task)
	if type(task) == "string" then
		table.insert(self._tasks, {
			name = task
		})
	else
		table.insert(self._tasks, task)
	end
end

function Room:add_branch()
	local r = Room:new()
	self:attach_branch(r)
	return r
end

function Room:attach_branch(other)
	assert(other, "Room:attach_branch requires a room to attach")
	assert(not other._parenet, "Cannot reparent a room")

	table.insert(self._connections, other)
	other._parent = self
end

function Room:tasks()
	return self._tasks
end

-- Adds a doorway between this room and [room]. The exact details (where along the matching edge the door is
-- placed) is determined randomly.
--
-- Additionally, mission data may be specified in the [mission_data] argument. This is a table which marks this
-- door as being important to mission progression (so a locked door will prevent movement from the other room to
-- this room, but not necessaraly the other way around) and can supply additional information about how the
-- door should be generated in game.
--
-- For more information on the door generation, see ZoneFuser:_generate_connecting_room
function Room:add_doorway_to(room, mission_data)
	local intersections = RoomList.find_intersections(self, room)

	-- If one or both of the rooms have doorway restrictions, apply those
	if self.door_connection_points or room.door_connection_points then
		local function point_in_list(room, x, y)
			if not room.door_connection_points then
				return true
			end
			for _, point in ipairs(room.door_connection_points) do
				if room.x + point[1] == x and room.y + point[2] == y then
					return true
				end
			end
			return false
		end

		local raw_intersections = intersections
		intersections = {}
		while #raw_intersections > 0 do
			local idx = math.random(#raw_intersections)
			local item = table.remove(raw_intersections, idx)

			if point_in_list(self, room.x + item.other_x, room.y + item.other_y) and
				point_in_list(room, self.x + item.room_x, self.y + item.room_y) then
				table.insert(intersections, item)
			end
		end
	end

	-- If there's no intersections, fail - this will be caused by doorway restrictions
	if #intersections == 0 then
		return false
	end

	-- Try to avoid corner doorways
	local i
	if #intersections > 2 then
		i = intersections[math.random(#intersections - 2) + 1]
	else
		i = intersections[math.random(#intersections)]
	end

	table.insert(self._doors, {
		other=room,
		norm = i.room_norm,
		x = i.room_x,
		y = i.room_y,
		mission_data = mission_data,
	})

	table.insert(room._doors, {
		other=self,
		norm = i.other_norm,
		x = i.other_x,
		y = i.other_y,
		other_mission_data = mission_data,
	})

	return true
end

-- PD2 only: get the room generator, or nil for the default
function Room:pd2_generator()
	return self._pd2_generator
end

return Room
