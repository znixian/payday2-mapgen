local RoomList = class()

function RoomList:init(rooms)
	self._rooms = rooms
	self:recalculate()
end

function RoomList:recalculate()
	assert(#self._rooms > 0, "RoomList must have at least one room!")

	self._minx = self._rooms[1].x
	self._miny = self._rooms[1].y

	self._maxx = self._rooms[1].x
	self._maxy = self._rooms[1].y

	for _, room in ipairs(self._rooms) do
		self._minx = math.min(self._minx, room.x)
		self._miny = math.min(self._miny, room.y)

		self._maxx = math.max(self._maxx, room.x + room.w - 1)
		self._maxy = math.max(self._maxy, room.y + room.d - 1)
	end
end

function RoomList:recenter(rx, ry)
	rx = rx or 1
	ry = ry or 1

	for _, room in ipairs(self._rooms) do
		room.x = room.x - self._minx + rx
		room.y = room.y - self._miny + ry
	end

	self:recalculate()
end

-- Check if a given room overlaps any room inside this roomlist (excluding itself).
-- If a room does overlap it, it will return true and that room (if multiple rooms overlap it, only the first will be returned)
-- If nothing overlaps it, false and nil will be returned
function RoomList:any_overlap(r)
	for _, room in ipairs(self._rooms) do
		if room:overlaps(r) and room ~= r then
			return true, room
		end
	end

	return false, nil
end

-- Check if a given room contains the given point
-- If a room does contain it, it will return true and that room (if multiple rooms contain it, only the first will be returned)
-- If nothing overlaps it, false and nil will be returned
function RoomList:any_contain(x, y)
	for _, room in ipairs(self._rooms) do
		if room:contains(x, y) then
			return true, room
		end
	end

	return false, nil
end

function RoomList:recalculate_neighbours_for(room)
	-- Wipe out the existing neighbour connections
	-- TODO setup unit testing for this, and other similar functions that can be run without the game
	for _, neighbour in ipairs(room._neighbours) do
		local other = neighbour.first
		if other == room then
			other = neighbour.second
		end
		for i=#other._neighbours,1,-1 do
			if other._neighbours[i] == neighbour then
				table.remove(other._neighbours, i)
			end
		end
	end
	room._neighbours = {}

	self:_recalculate_neighbours_internal(room, 1)
end

function RoomList:recalculate_all_neighbours()
	for _, room in ipairs(self._rooms) do
		room._neighbours = {}
	end

	for i, room in ipairs(self._rooms) do
		self:_recalculate_neighbours_internal(room, i + 1)
	end
end

function RoomList:_recalculate_neighbours_internal(room, start_idx)
	for i = start_idx, #self._rooms do
		local other = self._rooms[i]
		if
			other:intersects(room.x - 1, room.y, 1, room.d) or
			other:intersects(room.x, room.y - 1, room.w, 1) or
			other:intersects(room.x + room.w, room.y, 1, room.d) or
			other:intersects(room.x, room.y + room.d, room.w, 1) then

			local neighbour_pair = {
				first = room,
				second = other
			}

			table.insert(room._neighbours, neighbour_pair)
			table.insert(other._neighbours, neighbour_pair)
		end
	end
end

-- Static helper functions

function RoomList._find_edge_intersections(intersections, room, other, x, y, w, d, ox, oy)
	-- TODO optimise this
	for xx = 0, w - 1 do
		for yy = 0, d - 1 do
			if other:contains(x + xx, y + yy) then
				table.insert(intersections, {
					other_x=x+xx - other.x,
					other_y=y+yy - other.y,
					other_norm = { x=ox, y=oy },

					room_x=x+xx+ox - room.x,
					room_y=y+yy+oy - room.y,
					room_norm = { x=-ox, y=-oy },
				})
			end
		end
	end
end

function RoomList.find_intersections(room, other)
	local intersections = {}
	RoomList._find_edge_intersections(intersections, room, other, room.x - 1, room.y, 1, room.d, 1, 0)
	RoomList._find_edge_intersections(intersections, room, other, room.x, room.y - 1, room.w, 1, 0, 1)
	RoomList._find_edge_intersections(intersections, room, other, room.x + room.w, room.y, 1, room.d, -1, 0)
	RoomList._find_edge_intersections(intersections, room, other, room.x, room.y + room.d, room.w, 1, 0, -1)
	return intersections
end

return RoomList
