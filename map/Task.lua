local Task = class()

function Task:init(name)
	self._name = name
	self._subs = {}
end

function Task:name()
	return self._name
end

function Task:subs()
	return self._subs
end

function Task:add_sub(name)
	local sub = Task:new(name)
	table.insert(self._subs, sub)
	return sub
end

return Task
