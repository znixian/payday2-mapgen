local Zone = class()

function Zone:init()
	self._children = {}
end

function Zone:add_subzone()
	local sub = Zone:new()
	sub._parent = self
	table.insert(self._children, sub)
	return sub
end

function Zone:children()
	return self._children
end

return Zone
