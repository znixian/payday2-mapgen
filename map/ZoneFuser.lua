local Room = require("Room")
local RoomList = require("RoomList")
local IntraZoneLayout = require("IntraZoneLayout") -- TEMPORARY!
local Spec = require("spec/Spec")

local ZoneFuser = class()

function ZoneFuser:fuse(zone)
	assert(#zone:children() >= 1, "Cannot fuse childless zone!")

	local roomlist = RoomList:new(zone.rooms)
	roomlist:recenter()

	local child_rooms = {}
	for _, child in ipairs(zone:children()) do
		local cr

		if #child:children() > 0 then
			cr = self:fuse(child)
		else
			cr = RoomList:new(child.rooms)
			cr:recenter()
		end

		table.insert(child_rooms, cr)
	end

	assert(#zone:children() == 1, "multiple child zones are not yet supported!")
	assert(#child_rooms == 1, "multiple child zones are not yet supported!")

	local child = child_rooms[1]

	dprint("fusing " .. tostring(zone.id))

	for _, room in ipairs(child._rooms) do
		table.insert(roomlist._rooms, room)
	end

	child:recenter(roomlist._maxx + 1, roomlist._miny)

	local connecting_room = self:_generate_connecting_room(roomlist, child)
	-- TODO add a drill door or whatever into the connecting room

	roomlist:recalculate()

	return roomlist
end

function ZoneFuser:_generate_connecting_room(roomlist, child)
	-- Now look down the line between the two areas for the best space to place a joiner room
	-- First, calculate the sizes of the gaps along that area
	local spacings = {}
	for y=1,roomlist._maxy do
		local freeleft = 0
		while not roomlist:any_contain(roomlist._maxx - freeleft, y) do
			freeleft = freeleft + 1

			if freeleft > 100 then
				freeleft = nil
				break
			end
		end

		local freeright = 0
		while not roomlist:any_contain(roomlist._maxx + 1 + freeright, y) do
			freeright = freeright + 1

			if freeright > 100 then
				freeright = nil
				break
			end
		end

		if freeleft ~= nil and freeright ~= nil then
			spacings[y] = {freeleft, freeright}
		end
	end

	-- Generate a joining room
	local room = Spec:random_join_room():build_room()

	-- Choose the place with the best gap
	-- Do this by walking through the gaps and finding the first one with the best match to our desired width
	-- If we can't find a gap wide enough, we just move the next room across later
	-- Note that the major flaw in this is that we pay no attention to how much vertical room is available, so it's
	-- completely possible to get one-cell-wide rooms. This isn't the end of the world, but it would be nice to avoid.
	local best, best_score
	for y, data in pairs(spacings) do
		-- Produce a score based on how large the gap is
		local score = data[1] + data[2] - room.w

		-- If the gap is too small, only knock half the points - we can make the gap larger later, but not smaller
		if score < 0 then
			-- (also flip it to positive while we're at it)
			score = score * -0.5
		end

		local contender = not best or best_score > score

		-- Make sure there's enough vertical space for this room
		-- Only need to look downwards, since any point above us will
		-- have already considered our current position.
		if contender then
			local available_height = 1
			while available_height < room.d do
				local cell_data = spacings[y + available_height]
				if not cell_data then
					break
				end
				-- If there's less space on either side, we can't expand to there
				if cell_data[1] < data[1] or cell_data[2] < data[2] then
					break
				end
				available_height = available_height + 1
			end
			if available_height < room.d then
				contender = false
			end
		end

		-- And keep track of what Y position has thus far scored the best
		if contender then
			best_score = score
			best = y
		end
	end

	-- Find how wide the gap is at the 'best' (as determined above) point
	local gap_width = spacings[best][1] + spacings[best][2]

	-- And if it's not as wide as we'd like, make it wider by moving the child zone
	if gap_width < room.w then
		local shift_width = room.w - gap_width
		child:recenter(roomlist._maxx + 1 + shift_width, roomlist._miny)
		gap_width = room.w
	end

	-- Create the joining room
	room.x = roomlist._maxx - spacings[best][1] + 1
	room.y = best
	room.w = gap_width -- May have grown
	-- room.d was enforced in the spacing search
	room.marker = "!" -- filler character in the debugging view, make the room stand out a bit
	table.insert(roomlist._rooms, room)

	-- Calculate the neighbours for this new room
	roomlist:recalculate_neighbours_for(room)

	-- Mission info about the door, will be read later in RoomGen when generating the actual, physical door
	-- We should put stuff like what type of door it is - whether it can be drilled or opened by a computer/key/other objective, and if so
	-- then which task it corresponds to.
	local mission_data = {
		-- The door will look different on the map, to aid in out-of-game development
		map_char = "@",
	}

	-- Add the doors - one door to a room on each side
	local door_side_count = { 0, 0 }
	for _, neighbour in ipairs(room._neighbours) do
		assert(neighbour.first == room) -- Since we just called recalculate_neighbours_for on it, 'second' will always refer to the other room
		local other = neighbour.second

		-- Test if the room is in the child zone or not
		local is_to = false
		for _, r in ipairs(child._rooms) do
			if r == other then
				is_to = true
				break
			end
		end

		-- Add the door if there isn't already one on that side
		local door_side_idx = is_to and 2 or 1
		if door_side_count[door_side_idx] == 0 then
			-- The doors leading to the parent zones (which the player has access to first) need to be locked in some way
			room:add_doorway_to(other, not is_to and mission_data or nil)
			door_side_count[door_side_idx] = door_side_count[door_side_idx] + 1
		end
	end

	-- Ensure that we have got a door to either side, otherwise the mission will be impossible to complete
	assert(door_side_count[1] == 1)
	assert(door_side_count[2] == 1)

	return room
end

return ZoneFuser
