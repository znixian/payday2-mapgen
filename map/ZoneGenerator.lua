local Objectives = require("objectives")
local Zone = require("Zone")

local ZoneGenerator = class()

function ZoneGenerator:generate(mission)
	local root = Zone:new()
	local zones = { root }

	local function zonify(zone, task)
		assert(zone)
		assert(task)

		local name = task:name()
		local info = assert(Objectives.types[name], "Cannot find info for task: " .. name)

		if info.zoning then
			zone = zone:add_subzone()
			table.insert(zones, zone)
		end

		for _, sub in ipairs(task:subs()) do
			zonify(zone, sub)
		end
	end

	zonify(root, mission.start)

	-- Debugging
	if dprint_enabled then
		local function log_zone(zone, padding)
			padding = padding or ""
			dprint(padding .. "Zone")

			for _, sub in ipairs(zone:children()) do
				log_zone(sub, padding .. "  ")
			end
		end

		log_zone(root)
	end

	return zones
end

return ZoneGenerator
