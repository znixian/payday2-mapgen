if _G.ProcedualMapGen._mapenv then
	return _G.ProcedualMapGen._mapenv
end

local mapenv = {}
setmetatable(mapenv, {
	__index = _G
})

mapenv._G = mapenv
mapenv._GG = _G
mapenv._modules = {}

mapenv.SysEnv = {
	path_base = ProcedualMapGen.ModPath,
}

mapenv.loadfile = blt.vm.loadfile

-- debug printing
mapenv.dprint_enabled = false
mapenv.dprint = mapenv.dprint_enabled and log or function()end

function mapenv.require(name)
	if mapenv._modules[name] then
		return mapenv._modules[name]
	end

	local func = assert(blt.vm.loadfile(ProcedualMapGen.ModPath .. "map/" .. name .. ".lua"))
	setfenv(func, mapenv)
	local mod = func()
	mapenv._modules[name] = mod
	return mod
end

ProcedualMapGen.Spec = mapenv.require("spec/Spec")

return mapenv.require("Generator")

