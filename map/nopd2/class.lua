local __overrides = {}
local __everyclass = {}

return function(...)

        local super = (...)
        if select("#", ...) >= 1 and super == nil then
                error("trying to inherit from nil", 2)
        end
        local class_table = {}
        if __everyclass then
                table.insert(__everyclass, class_table)
        end
        class_table.super = __overrides[super] or super
        class_table.__index = class_table
        class_table.__module__ = getfenv(2)
        setmetatable(class_table, __overrides[super] or super)

        function class_table.new(klass, ...)
                local object = {}
                setmetatable(object, __overrides[class_table] or class_table)
                if object.init then
                        return object, object:init(...)
                end
                return object
        end

        return class_table

end

