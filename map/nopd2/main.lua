math.randomseed(1234)

local relpath = "../"

local mapenv = {}
setmetatable(mapenv, {
        __index = _G
})

mapenv._G = mapenv
mapenv._GG = _G
mapenv._modules = {}

mapenv.SysEnv = {
	path_base = relpath .. "../",
}

mapenv.class = dofile("class.lua")
dofile("maths.lua")
mapenv.log = print
mapenv.loadfile = loadfile

-- debug printing
mapenv.dprint_enabled = true

mapenv.dprint = mapenv.dprint_enabled and print or function()end

function mapenv.require(name)
        if mapenv._modules[name] then
                return mapenv._modules[name]
        end

        local func = assert(loadfile(relpath .. name .. ".lua"))
        setfenv(func, mapenv)
        local mod = func()
        mapenv._modules[name] = mod
        return mod
end

local gen = mapenv.require("Generator")
local maps = gen:generate()
