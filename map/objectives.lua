local Objectives = {}

local types = {}
Objectives.types = types

types.delay = {
	zoning = true,
	multiple = false,
}

types.key_door = {
	zoning = true,
	multiple = true,
}

types.entry = {
}

types.loot = {
}

types.key = {
}

local instances = {}
Objectives.instances = instances
instances.drill_light_door = {
	type = "delay",
}
instances.drill_vault_door = {
	type = "delay",
}
instances.drill_safe_loot = {
	type = "loot",
	effect = "physical",
}
instances.drill_safe_key = {
	type = "key",
	effect = "physical",
}

instances.explosive_vault_door = {
	type = "delay",
}

instances.hack_mainframe = {
	type = "key",
	effect = "scripted",
}
instances.hack_laptop = {
	type = "key",
	effect = "scripted",
}
instances.security_room = {
	type = "key",
	effect = "scripted",
}

instances.key_vault = {
	type = "key_door",
}

instances.bars = {
	type = "delay",
}

instances.thermite_floor = {
	type = "delay",
}
instances.thermite_grate = {
	type = "delay",
}

instances.gold = {
	type = "loot",
}

return Objectives
