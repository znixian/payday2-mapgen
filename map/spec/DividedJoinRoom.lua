-- This room is one that's split in two by some obstacle, which is configurable
-- This might be a vault door, C4-able wall, etc.
-- Note it's sizes are in metres, because it's expected they will be closely tied
--  to the size of specific units.

local Room = require("Room")
local RoomSpec = require("spec/RoomSpec")
local Constants = require("Constants")

local Spawner = class()

-- In-game only function, replacement for RoomGen:generate
function Spawner:generate(room, world_def)
	-- Generate most of the room normally, we'll then add the division
	ProcedualMapGen.RoomGen:generate(room, world_def)

	local cell_size = Constants.cell_size

	local origin_tbl = room._spec._data.prefab_origin or {}
	local origin = Vector3(origin_tbl[1] or 0, origin_tbl[2] or 0, origin_tbl[3] or 0)

	self._pos = Vector3()
	local rot = Rotation(math.UP, origin_tbl.rotation or 0)

	mvector3.set_static(self._pos, (room.x + room.w/2) * cell_size, room.y * cell_size)
	self._units = ProcedualMapGen.PrefabLoader:spawn(assert(room._spec._data.path), self._pos, rot, origin)
end

function Spawner:generate_scripts(room, mission_manager)
	-- FIXME generalise this in some way
	local c4s = {
		"c4_plantable_002",
		"c4_plantable_003",
		"c4_plantable_004",
		"c4_plantable_005",
		"c4_plantable_006",
	}
	local unit_ids = {}
	local trigger_list = {}
	for i, name in ipairs(c4s) do
		local unit_id = self._units[name]:unit_data().unit_id
		assert(unit_id and unit_id ~= 0)
		unit_ids[i] = unit_id

		trigger_list[i] = {
			name = "run_sequence",
			notify_unit_id = unit_id,
			notify_unit_sequence = "trigger_expolsive",
		}
	end

	local main_wall = self._units["hcm_int_wall_4m_a_001"]
	local explosive_wall = self._units["hcm_int_wall_explosion_001"]:unit_data().unit_id
	assert(explosive_wall and explosive_wall ~= 0)

	mission_manager:run_element(CoreElementUnitSequence.ElementUnitSequence, {
		trigger_list = {{
			name = "run_sequence",
			notify_unit_id = explosive_wall,
			notify_unit_sequence = "state_vis_hide",
		}},
		position = Vector3(),
		trigger_times = 1,
	})

	local delete_wall = mission_manager:create_element(ProcedualMapGen.LuaCallbackElement, {
		position = Vector3(),
		trigger_times = 1,
		callback = function()
			World:delete_unit(main_wall)
			main_wall = nil
		end
	})
	local explosion_effect = mission_manager:create_element(CoreElementPlayEffect.ElementPlayEffect, {
		position = self._pos,
		trigger_times = 1,
		effect = "effect/particles/explosions/explosion_grenade_launcher",
	})
	local play_animation = mission_manager:create_element(CoreElementUnitSequence.ElementUnitSequence, {
		trigger_list = {{
			name = "run_sequence",
			notify_unit_id = explosive_wall,
			notify_unit_sequence = "state_explode_x_positive",
		}},
		position = Vector3(),
		trigger_times = 1,
		on_executed = {{id=delete_wall:id(), delay=0}},
	})

	local start_c4_ticking = mission_manager:create_element(CoreElementUnitSequence.ElementUnitSequence, {
		trigger_list = trigger_list,
		position = Vector3(),
		trigger_times = 1,
	})
	local counter = mission_manager:create_element(CoreElementCounter.ElementCounter, {
		counter_target = 1, -- TESTING should be #c4s,
		position = Vector3(),
		trigger_times = 1,
		on_executed = {
			{id=start_c4_ticking:id(), delay=0},
			{id=play_animation:id(), delay=5},
		},
	})

	for _, unit_id in ipairs(unit_ids) do
		mission_manager:create_element(CoreElementUnitSequenceTrigger.ElementUnitSequenceTrigger, {
			unit_id = unit_id,
			sequence = "interact",
			on_executed = {{id=counter:id(), delay=0}},
			trigger_times = 1,
		})
	end
end

-- Note: size is {x,y} in metres
local DividedJoinRoom = class(RoomSpec)

function DividedJoinRoom:build_room()
	local room = Room:new()

	-- Convert from metres to world units
	-- This is done so adjusting the cell size won't muck up the prefab definitions too much
	local scaling = 100 / Constants.cell_size

	local x = math.floor(self._data.size.x * scaling)
	local y = math.floor(self._data.size.y * scaling)

	room.w = x
	room.d = y

	room._spec = self
	room._pd2_generator = Spawner:new()

	-- Don't also put on a mission door
	function room:add_doorway_to(room, mission_data)
		-- Leave out the mission data, since we're setting up an internal obstacle
		Room.add_doorway_to(self, room)
	end

	return room
end

return DividedJoinRoom
