local Prefab = class(require("spec/UnitSpec"))

-- In-game only functions
function Prefab:pd2_spawn(pos, rot)
	-- Calculate the position offset from rotation around the centre of
	-- the object (from it's footprint).
	local centre_rotation = Rotation(math.UP, self._data.centre_rotation or 0)
	local origin_from_centre = -self._footprint / 2
	local rotated_origin = origin_from_centre:rotate_with(centre_rotation)
	pos = pos + (rotated_origin - origin_from_centre)

	rot = rot * centre_rotation

	if self._position then
		pos = pos + self._position:rotate_with(rot)
	end

	ProcedualMapGen.PrefabLoader:spawn(assert(self._data.path), pos, rot)
end

return Prefab
