local Room = require("Room")
local RoomSpec = require("spec/RoomSpec")
local Constants = require("Constants")

local Spawner = class()

function Spawner:init(rotated)
	self.rotated = rotated
end

function Spawner:generate(room, world_def)
	-- In-game only function, replacement for RoomGen:generate

	local cell_size = Constants.cell_size

	local origin_tbl = room._spec._data.prefab_origin or {}
	local origin = Vector3(origin_tbl[1] or 0, origin_tbl[2] or 0, origin_tbl[3] or 0)

	local rotation = (self.rotated and 90 or 0) + (origin_tbl.rotation or 0)

	local post_offset_tbl = room._spec._data.post_rotate_offset or {}
	local post_offset = Vector3(post_offset_tbl[1] or 0, post_offset_tbl[2] or 0, post_offset_tbl[3] or 0)

	local pos = Vector3()
	local rot = Rotation(math.UP, rotation)

	mvector3.set_static(pos, room.x * cell_size, room.y * cell_size)

	ProcedualMapGen.PrefabLoader:spawn(assert(room._spec._data.path), pos + post_offset, rot, origin)
end

function Spawner:generate_scripts(room, mission_manager)
end

-- Note: size is {x,y} in metres
local RoomPrefab = class(RoomSpec)

function RoomPrefab:build_room()
	local room = Room:new()

	-- Convert from metres to world units
	-- This is done so adjusting the cell size won't muck up the prefab definitions too much
	local scaling = 100 / Constants.cell_size

	local x = math.floor(self._data.size[1] * scaling)
	local y = math.floor(self._data.size[2] * scaling)

	local rotated = math.random() > 0.5 and false
	if rotated then
		room.w = x
		room.d = y
	else
		room.w = y
		room.d = x
	end

	room.door_connection_points = self._data.doors

	room._spec = self

	room._pd2_generator = Spawner:new(rotated)

	return room
end

return RoomPrefab
