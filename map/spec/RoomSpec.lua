local Room = require("Room")

local function rand_range(range)
	return math.random(range[1], range[2])
end

local RoomSpec = class()

function RoomSpec:init(name, data)
	self._name = name
	self._data = data
end

function RoomSpec:name()
	return self._name
end

-- Ultimately weights will be set by the biome
function RoomSpec:weight()
	return self._data.weight or 1
end

function RoomSpec:spawn_limit()
	return self._data.spawn_limit or 100000
end

function RoomSpec:build_room()
	local room = Room:new()

	local major = rand_range(self._data.size.major)
	local minor = rand_range(self._data.size.minor)

	if math.random() > 0.5 then
		room.w = major
		room.d = minor
	else
		room.w = minor
		room.d = major
	end

	room._spec = self

	return room
end

return RoomSpec
