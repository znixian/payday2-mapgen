local Spec = class()

local function exec(name)
	local dir = SysEnv.path_base .. "spec/"
	local f = assert(loadfile(dir .. name))
	local env = {
		RoomSpec = require("spec/RoomSpec"),
		RoomPrefab = require("spec/RoomPrefab"),
		DividedJoinRoom = require("spec/DividedJoinRoom"),
		UnitSpec = require("spec/UnitSpec"),
		UnitGroup = require("spec/UnitGroup"),
		Prefab = require("spec/Prefab"),
	}
	setfenv(f, env)
	return assert(f())
end

function Spec:init()
	local def = exec("spec.lua")

	self._rooms = {}
	self._join_rooms = {}
	self._units = {}
	self._room_counts = {}
	for _, file in ipairs(def.include) do
		local data = exec(file)
		for _, room in ipairs(data.rooms) do
			table.insert(self._rooms, room)
		end
		for _, room in ipairs(data.join_rooms) do
			table.insert(self._join_rooms, room)
		end
		for _, unit in ipairs(data.units) do
			assert(not self._units[unit:name()], "Duplicate unit " .. unit:name())
			self._units[unit:name()] = unit
		end
	end
end

-- In the future this will be split per biome, but those don't exist yet
function Spec:random_room()
	return self:_random_room_impl(self._rooms)
end

-- Get a room that can join two areas with a mission door
function Spec:random_join_room()
	return self:_random_room_impl(self._join_rooms)
end

function Spec:_random_room_impl(list)
	local weight_points = {}
	local curr_weight = 0

	for _, room in ipairs(list) do
		curr_weight = curr_weight + room:weight()
		table.insert(weight_points, curr_weight)
	end

	local value = math.random() * curr_weight
	for i, max in ipairs(weight_points) do
		if value >= max then
			goto next
		end
		local room = list[i]

		local current_count = self._room_counts[room:name()] or 0
		if current_count >= room:spawn_limit() then
			goto next
		end
		self._room_counts[room:name()] = current_count + 1

		if true then
			return room
		end

		::next::
	end

	error("No rooms, or weird float error")
end

function Spec:get_unit(name)
	unit = self._units[name]
	if not unit then
		error("No such unit " .. name)
	end
	return unit:resolve()
end

return Spec:new()
