local UnitGroup = class(require("spec/UnitSpec"))

-- In-game only functions
function UnitGroup:pd2_spawn(pos, rot)
	local units = assert(self._data.units)

	-- Can only require this now, since we're required by Spec and otherwise we'd get infinite recursion
	local Spec = require("spec/Spec")

	for _, udata in ipairs(units) do
		local po = udata.offset
		local ro = udata.rotation
		local unit = Spec:get_unit(udata.unit)
		unit:pd2_spawn(pos + Vector3(po[1], po[2], po[3]), Rotation(ro[1], ro[2], ro[3]) * rot)
	end
end

return UnitGroup
