local UnitSpec = class()

function UnitSpec:init(name, data)
	self._name = name
	self._data = data

	if self._data.position then
		self._position = Vector3(unpack(self._data.position))
	end

	if self._data.footprint then
		self._footprint = Vector3(unpack(self._data.footprint))
	end
end

function UnitSpec:name()
	return self._name
end

-- If this UnitSpec is a randomiser, pick and return one unit
function UnitSpec:resolve()
	return self
end

-- TODO generalise for other things
function UnitSpec:is_corner()
	return self._data.corner_placement
end

-- In-game only functions
function UnitSpec:pd2_spawn(pos, rot)
	local id = assert(self._data.unit)
	if type(id) == "table" then
		id = id[math.random(#id)]
	end
	assert(type(id) == "string")

	if self._position then
		pos = pos + self._position:rotate_with(rot)
	end

	local unit = World:spawn_unit(Idstring(id), pos, rot)
end

return UnitSpec
