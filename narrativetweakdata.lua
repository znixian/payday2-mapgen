Hooks:PostHook(NarrativeTweakData, "init", "AddMapgenNarrativeData", function(self)
	self.stages.mapgen_1 = {
		-- type = "d",
		type_id = "heist_type_assault",
		level_id = "mapgen" -- "nightclub"
	}

	self.jobs.mapgen = {
		name_id = "heist_mapgen_name",
		briefing_id = "heist_mapgen_brief",
		contact = "bain",

		chain = {
			self.stages.mapgen_1
		},

		-- Job Class
		-- See Appendix - Experience in the Long Guide
		jc = 50,

		payout = {0.001,0.001,0.001,0.001,0.001},
		contract_cost = {0,0,0,0,0},
		experience_mul = {0.001,0.001,0.001,0.001,0.001},
		contract_visuals = {
			min_mission_xp = 0.001, -- {0.001,0.001,0.001,0.001,0.001},
			max_mission_xp = 0.001, -- {0.001,0.001,0.001,0.001,0.001},
			preview_image = {icon = icon}
		},
		ignore_heat = true,
	}

	--[[
	local nils = {
		job_wrapper = true,
		wrapped_to_job = true,
		competitive = true,
		hidden = true,
		ignore_statistics = true,
		dlc = true,
		professional = true,
	}

	setmetatable(self.jobs.mapgen, {
		__index = function(_, name)
			if nils[name] then return nil end

			-- error(name)
			log(name)
		end
	})
	--]]

	table.insert(self._jobs_index, "mapgen")

	-- afaik don't need this?
	-- self:set_job_wrappers()
end)

