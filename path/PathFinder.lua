local PathFinder = class()

local grid_size = 200
local nav_grid_size

local function mod_to_nav(val)
	nav_grid_size = nav_grid_size or managers.navigation._grid_size

	return val * grid_size / nav_grid_size
end

-- Returns a value to be passed to managers.navigation:set_load_data
function PathFinder:build(roomlist)
	-- dprint("Built AI data")

	local data = {}

	local table_new = function() return {} end -- require("table.new")

	-- the X and Y coordinates of the room
	data.room_borders_x_pos = table_new(#roomlist, 0)
	data.room_borders_x_neg = table_new(#roomlist, 0)
	data.room_borders_y_pos = table_new(#roomlist, 0)
	data.room_borders_y_neg = table_new(#roomlist, 0)

	-- The Z coordinates for each corner of the room
	data.room_heights_xn_yn = table_new(#roomlist, 0)
	data.room_heights_xn_yp = table_new(#roomlist, 0)
	data.room_heights_xp_yn = table_new(#roomlist, 0)
	data.room_heights_xp_yp = table_new(#roomlist, 0)

	-- door support
	data.door_low_pos = {}
	data.door_high_pos = {}
	data.door_low_rooms = {}
	data.door_high_rooms = {}

	-- Room visibility groups
	data.room_vis_groups = table_new(#roomlist, 0)

	local gen_info = {}
	gen_info.door_surfaces = {}

	for id, room in ipairs(roomlist) do
		self:_build_room(roomlist, data, id)
	end

	for id, room in ipairs(roomlist) do
		self:_build_room_doors(roomlist, data, id, gen_info)
	end

	-- Nav segments
	data.nav_segments = self:_build_nav_segments(roomlist)

	-- Visibility groups
	data.vis_groups = self:_build_vis_groups(roomlist, data, gen_info)

	-- Couldn't be set up in build_nav_segments, since we need the vis groups first
	self:_build_surface_neighbours(roomlist, data, gen_info)

	data.version = NavFieldBuilder._VERSION

	-- Cover data
	local cover_data = self:_build_cover_data(roomlist)

	return data, cover_data
end

function PathFinder:_build_room(roomlist, data, room_id)
	local m2n = mod_to_nav
	local room = roomlist[room_id]

	local pos = {x=m2n(room.x) + 1, y=m2n(room.y) + 1}
	local size = {x=m2n(room.w) - 2, y=m2n(room.d) - 2}
	local nav_id = self:_add_nav_room(data, pos, size)

	-- for our internal use - this isn't somehow used by PD2
	room._navdata = {
		nav_id = nav_id
	}
end

function PathFinder:_build_room_doors(roomlist, data, room_id, gen_info)
	local m2n = mod_to_nav
	local room = roomlist[room_id]
	local nav_id = room._navdata.nav_id

	for _, door in ipairs(room._doors) do
		local other_id = door.other._navdata.nav_id
		if door.norm.x > 0 or door.norm.y > 0 then

			-- Find the coordinates the door should be placed on
			-- Note that the navgen specifies what interior cell the door attaches
			-- to, not the position of the doorway. This means that if we don't do anything
			-- else, then doorways will be one cell behind the walls for the +x and +y sides
			-- of the room. To counter this, add the normal component if it's positive.
			local x = room.x + door.x + math.max(0, door.norm.x)
			local y = room.y + door.y + math.max(0, door.norm.y)

			-- Find the gap size, and subtract one cell
			local gap_size = 100 / nav_grid_size - 2
			local gap_offset = 50 / nav_grid_size + 1

			-- Swap the X and Y components if the door is facing in the X direction
			local function xswap(pos, off)
				pos = pos or {x=0,y=0}
				if door.norm.x == 0 then
					return {x=pos.x+off.x, y=pos.y+off.y}
				else
					return {x=pos.x+off.y, y=pos.y+off.x}
				end
			end

			-- Door length along the X axis, facing into the Y axis
			local bridge_pos = xswap({x=m2n(x), y=m2n(y)}, {x=gap_offset, y=-1})

			local room_len = 2

			-- Make a room to bridge the two rooms together, since the nav planes don't extend to the very edge due to the margin.
			-- This stops cops from walking to the door, then walking out and clipping through the model if their objective is beside the door.
			local bridge = self:_add_nav_room(data, bridge_pos, xswap(nil, {x=gap_size, y=room_len}))

			local length = xswap(nil, {x=gap_size, y=0})
			self:_add_nav_door(data, bridge_pos, length, nav_id, bridge)
			self:_add_nav_door(data, xswap(bridge_pos, {x=0, y=room_len}), length, other_id, bridge)

			gen_info.door_surfaces[bridge] = { door=door }
		end
	end
end

function PathFinder:_add_nav_room(data, pos, size)
	local x, y, w, d = pos.x, pos.y, size.x, size.y
	-- TODO convert between managers.navigation._grid_size and our custom grid size
	table.insert(data.room_borders_x_neg, x)
	table.insert(data.room_borders_y_neg, y)

	-- TODO do these encounter fencepost errors
	table.insert(data.room_borders_x_pos, x + w)
	table.insert(data.room_borders_y_pos, y + d)

	-- Insert the heights
	-- TODO do we convert these?
	-- FIXME only adding one to avoid z-fighting for debugging
	local room_z = 22.5
	table.insert(data.room_heights_xn_yn, room_z)
	table.insert(data.room_heights_xn_yp, room_z)
	table.insert(data.room_heights_xp_yn, room_z)
	table.insert(data.room_heights_xp_yp, room_z)

	-- Don't add anything to room_vis_groups - that'll be filled out later

	return #data.room_borders_x_neg
end

function PathFinder:_add_nav_door(data, pos, size, r1, r2)
	local door_z = 22.5 -- FIXME use proper pos
	table.insert(data.door_low_pos, Vector3(pos.x, pos.y, door_z))
	table.insert(data.door_high_pos, Vector3(pos.x+size.x, pos.y+size.y, door_z))

	table.insert(data.door_low_rooms, math.min(r1, r2))
	table.insert(data.door_high_rooms, math.max(r1, r2))
end

function PathFinder:_build_nav_segments(rooms)
	-- these are actually nav surfaces
	local segs = {}

	for _, room in ipairs(rooms) do
		local seg = {}
		table.insert(segs, seg)

		-- Put the segment position somewhere in the room - this is mostly for debugging
		seg.pos = Vector3(room.x * grid_size + 200, room.y * grid_size + 200, 0)

		-- vis_groups will be filled out by _build_vis_groups
		seg.vis_groups = {}

		-- will be filled out by _build_surface_neighbours
		seg.neighbours = {}

		-- Make the info available for later use
		room._navdata.surface = { id=#segs, data=seg }
	end

	return segs
end

function PathFinder:_build_vis_groups(rooms, data, gen_info)
	local groups = {}

	-- Currently we just generate one visgroup per room, later we should do this properly

	for _, room in ipairs(rooms) do
		local grp = {}
		table.insert(groups, grp)
		local id = #groups

		-- Set what rooms (engine)/nav segment (editor) are contained inside this visibility group
		-- Note that the values in this table don't matter; the keys specify the navsegment IDs
		-- This is a particularly nasty trap if you're testing with only a single room, since the ID
		-- will be 1 for the nav_id, and if you insert it in as though it was a list the key for that will
		-- be 1, and it will work by accident, but break mysteriously if you add more rooms.
		grp.rooms = {}
		grp.rooms[room._navdata.nav_id] = true

		for seg_id, bridge in pairs(gen_info.door_surfaces) do
			if bridge.door.other == room then
				grp.rooms[seg_id] = true
			end
		end

		-- The other visibility groups one can see while standing here
		-- Not sure if this is used by AI, but it's definately used for the player rendering. Excluding something
		-- that should have been included is a major issue, since the player won't see any enemies in that visibility group
		grp.vis_groups = {}

		-- The surface that this vis groups is attached to
		grp.seg = room._navdata.surface.id

		-- Make the room include this visgroup
		table.insert(room._navdata.surface.data.vis_groups, id)
	end

	-- Populate the room_vis_groups table from the rooms in each visgroup
	-- This *is* duplicating data - the visgroups list what rooms they contain, so there's no real reason why
	-- this needs to be included in the map data. Notably, while (IIRC) it's dressed up by the navigation manager to
	-- be used by Diesel, it's not actually used at all. Instead, the roomlist on the vis groups is used to derive this.
	-- Nonetheless, it may be used by Lua, hence why we build it.
	for vg_id, vg in ipairs(groups) do
		for room, _ in pairs(vg.rooms) do
			data.room_vis_groups[room] = vg_id
		end
	end

	return groups
end

function PathFinder:_build_surface_neighbours(roomlist, data, gen_info)
	-- Build the neighbours - this is required for navigation between rooms and group spawning (since enemies won't spawn if they can't reach the player)

	-- Given a segment ID, it looks up the containing surface ID and surface object
	local function get_surface(segment_id)
		local id = data.vis_groups[data.room_vis_groups[segment_id]].seg
		return id, data.nav_segments[id]
	end

	-- Add a neighbour to a given surface. [neighbour] is the ID of the neighbour, [door_id] is the ID of the door
	local function add_neighbour(surface, neighbour, door_id)
		surface.neighbours[neighbour] = surface.neighbours[neighbour] or {}
		table.insert(surface.neighbours[neighbour], door_id)
	end

	-- Go through each door, and if that door bridges between two different surfaces, add a neighbour connection between them via that door
	for i=1,#data.door_low_pos do
		local id1, surf1 = get_surface(data.door_low_rooms[i])
		local id2, surf2 = get_surface(data.door_high_rooms[i])
		if id1 ~= id2 then
			add_neighbour(surf1, id2, i)
			add_neighbour(surf2, id1, i)
		end
	end
end

function PathFinder:_build_cover_data(roomlist)
	local cd = {}

	-- cd should contain tables, either having first a position then a yaw number

	-- for now, just put down a row of six cover points for testing
	for _, room in ipairs(roomlist) do
		for i=1,6 do
			table.insert(cd, {
				Vector3(room.x * grid_size + 100 + i * 100, room.y * grid_size + 200, 0),
				0, -- yaw
			})
		end
	end

	return cd
end

return PathFinder
