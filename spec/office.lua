return {
	units = {
		Prefab:new("office:desk_group", {
			path = "assets/prefabs/OfficeGroup.prefab_xml",
			position = {-317.5, -765, 0},
			footprint = {350, 550},
		}),
		Prefab:new("office:entrance_desk", {
			path = "assets/prefabs/FrontDesk.prefab_xml",
			position = {-950, 0, 0},
			footprint = {400, 200},
		}),
		Prefab:new("office:security_camera_station", {
			-- Notes from BLE:
			-- Corner is pointing in positive X/Y - prefab origin is the empty corner
			-- Corner X is 200, corner Y is 375, corner Z is 0
			-- Length in the X axis is exactly 200 units
			-- Length in Y axis of desks is 400, including rubbish bin is about 450 (445 is without any margin)
			-- Positive-most corner is 200,375
			-- Origin is thus 0,-75 (including the rubbish bin)
			path = "assets/prefabs/SecurityCameraCornerStation.prefab_xml",
			position = {0, 75},
			footprint = {200, 450},
			centre_rotation = 180,
			corner_placement = true,
		}),

		UnitSpec:new("office:picture", {
			unit = {
				"units/payday2/props/lxa_prop_hallway_pictures/lxa_prop_hallway_picture_01",
				"units/payday2/props/lxa_prop_hallway_pictures/lxa_prop_hallway_picture_04",
			},
			wall_mount = true,
		}),
		UnitSpec:new("office:copy_machine", {
			unit = "units/world/props/office/copy_machine/copy_machine_03",
			wall_mount = false,
			position = {0, -55, 0},
		}),
		UnitSpec:new("office:pot_plant", {
			unit = {
				"units/pd2_dlc2/props/bnk_prop_lobby_plant_dracaenafragrans/bnk_prop_lobby_plant_dracaenafragrans_a",
				"units/pd2_dlc2/props/bnk_prop_lobby_plant_dracaenafragrans/bnk_prop_lobby_plant_dracaenafragrans_b",
				"units/pd2_dlc2/props/bnk_prop_lobby_plant_dracaenafragrans/bnk_prop_lobby_plant_dracaenafragrans_c",

				"units/payday2/props/stn_prop_lobby_plant_a/stn_prop_lobby_plant_a",
				"units/payday2/props/stn_prop_lobby_plant_b/stn_prop_lobby_plant_b",
			},
			wall_mount = false,
			position = {0, -70, 0},
		}),
	},
	rooms = {
		-- Fax room, similar to that in the original (H&T) bank heist
		RoomSpec:new("office:fax", {
			size = {
				minor = {2, 2},
				major = {3, 5},
			},
			weight = 0.25,
		}),

		RoomSpec:new("office:desk_area", {
			size = {
				minor = {6, 8},
				major = {4, 7},
			},

			props = {
				"office:picture", "office:picture", "office:picture", "office:picture", "office:picture",
				"office:copy_machine",
				"office:pot_plant", "office:pot_plant", "office:pot_plant", "office:pot_plant", "office:pot_plant",
			},
			floor_props = {
				"office:desk_group",
			},
		}),

		RoomPrefab:new("office:lavatory", {
			path = "assets/prefabs/OfficeLavatory.prefab_xml",
			prefab_origin = {1480, 2428, 0, rotation=90},
			post_rotate_offset = {0, 600, 0},
			size = {6,11},
			doors = {
				{0,-1},
			},
			spawn_limit = 1,
		}),

		RoomSpec:new("office:lobby", {
			size = {
				major = {4, 6},
				minor = {3, 4},
			},
			floor_props = {
				"office:entrance_desk",
			},
		}),

		RoomSpec:new("office:security_room", {
			size = {
				major = {3, 4},
				minor = {2, 3},
			},
			floor_props = {
				"office:security_camera_station",
			},
			spawn_limit = 1,
		}),

		RoomSpec:new("office:dev_dummy", {
			size = {
				minor = {7,10},
				major = {7,10},
			}
		}),
	},
	join_rooms = {
		DividedJoinRoom:new("generic:c4_wall", {
			path = "assets/prefabs/ExplosiveWall.prefab_xml",
			prefab_origin = {200, 1000, 0},
			size = {
				-- Note these are in metres!
				x = 6, -- This may get stretched beyond what we indend depending on the mapgen circumstances
				y = 4, -- Always the same 'height', this is the direction perpendicular to the flow of the map
			},
		}),
	},
}
