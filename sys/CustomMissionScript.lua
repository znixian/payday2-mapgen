core:import("CoreElementGlobalEventTrigger")
core:import("CoreElementUnitSequenceTrigger")
core:import("CoreElementUnitSequence")
core:import("CoreElementCounter")
core:import("CoreElementPlayEffect")
core:import("CoreEvent")

local custom_mission_script = class(CoreEvent.CallbackHandler)

function custom_mission_script:init()
	CoreEvent.CallbackHandler.init(self)

	self._elements = {}
end
function custom_mission_script:is_debug()
	return false
end
function custom_mission_script:element(id)
	return assert(self._elements[id], "Cannot get element " .. tostring(id))
end
function custom_mission_script:debug_output(txt)
	log("[CustomMissionScript] Element Debug: " .. tostring(txt))
end
function custom_mission_script:add_save_state_cb(element_id)
end

-- This adds the delayed callback handling required for delays in on_executed
local mission_manager = class()

function mission_manager:init()
	self._script = custom_mission_script:new()
	self._auto_id = 1000
end

function mission_manager:update(dt)
	self._script:update(dt)
end

function mission_manager:exec_mission_stuff(world_def)
	self._world_def = world_def

	self:run_element(ElementPlayerSpawner, {
		position = self:_tmp_room_pos(1),
		rotation = Rotation(),
		state = "standard",
	})

	self:run_element(ElementDifficulty, {
		difficulty = 0.5,
	})

	-- Does nothing since we start in loud mode, left here more as a note than anything else
	self:run_element(ElementWhisperState, {
		state = true,
	})

	local global_event = self:create_element(ElementAiGlobalEvent, {
		wave_mode = "besiege",
		-- AI_event = "police_called", -- gets the gloabl trigger stuck in a loop
		blame = "metal_detector",
		editor_name = "ai_global"
	})

	self:run_element(CoreElementGlobalEventTrigger.ElementGlobalEventTrigger, {
		global_event = "police_called",
		on_executed = {
			{id=global_event:id(), delay=0},
		},
		debug = true,
		editor_name = "police_called_trigger"
	})

	-- Spawn a lone guard, for testing
	self:run_element(ElementSpawnEnemyDummy, {
		enemy = "units/payday2/characters/ene_security_3/ene_security_3",
		position = self:_tmp_room_pos(2),
		participate_to_group_ai = false,
	})

	local spawn_points = {}
	for i = 1,5 do
		local elem = self:create_element(ElementSpawnEnemyDummy, {
			enemy = "units/payday2/characters/ene_swat_1/ene_swat_1",
			position = self:_tmp_room_pos(3, i),
			amount = 0, -- TODO what does this do?
			accessibility = "any",
			interval = 5,
		})
		table.insert(spawn_points, elem:id())
	end

	local groups = {}
	for name, _ in pairs(tweak_data.group_ai.enemy_spawn_groups) do
		if name ~= "Phalanx" then
			table.insert(groups, name)
		end
	end

	local spawn_group = self:create_element(ElementSpawnEnemyGroup, {
		spawn_type = "ordered",
		elements = spawn_points,
		preferred_spawn_groups = groups,
		amount = 0,
		interval = 3,
	})

	self:run_element(ElementEnemyPreferedAdd, {
		spawn_groups = {spawn_group:id()},
	})

	-- Generate elements to handle navigation relating to doors, as bots would otherwise walk through them
	for _, door in ipairs(self._world_def._doors) do
		-- Disable navigation initially
		self:run_element(ElementAIGraph, {
			graph_ids = { assert(door.room._navdata.nav_id) },
			operation = "forbid_access",
		})

		local enabler = self:create_element(ElementAIGraph, {
			graph_ids = { assert(door.room._navdata.nav_id) },
			operation = "allow_access",
		})

		local unit_id = door.unit:unit_data().unit_id
		assert(unit_id and unit_id ~= 0)

		-- Note we don't need to actually run the element to make it take effect
		self:create_element(CoreElementUnitSequenceTrigger.ElementUnitSequenceTrigger, {
			unit_id = unit_id,
			sequence = "done_opened",
			on_executed = {{id=enabler:id(), delay=0}},
		})

		-- Finally, run the door enable sequence - this is required to show the interaction items, such as for C4 and drilling
		self:run_element(CoreElementUnitSequence.ElementUnitSequence, {
			trigger_list = {{
				name = "run_sequence",
				notify_unit_id = unit_id,
				notify_unit_sequence = "activate_door",
			}},
			trigger_times = 0,
			position = Vector3(),
		})
	end

	-- Add any custom room stuff
	for _, room in ipairs(self._world_def._roomlist._rooms) do
		local generator = room:pd2_generator() or ProcedualMapGen.RoomGen
		generator:generate_scripts(room, self)
	end
end

function mission_manager:run_element(class, data)
	local elem = self:create_element(class, data)
	elem:on_executed()
	return elem
end

function mission_manager:create_element(class, data)
	local id = data.id
	local editor_name = data.editor_name
	if not id then
		id = self._auto_id
		self._auto_id = self._auto_id + 1
	end

	local values = {
		enabled = true,
		trigger_times = 0,
		base_delay = 0,
		on_executed = {},
	}

	for k, v in pairs(data) do
		values[k] = v
	end

	-- Stored in the same values table for use above, but not part of the desired element values
	values.id = nil
	values.editor_name = nil

	local elem = class:new(self._script, {
		id = id,
		editor_name = editor_name,
		values = values,
	})

	self._script._elements[id] = elem

	-- Activate the element, in particular AI elements often look up elements specified by ID. Future
	-- units won't yet be loaded so that'll fail if the user forward-references a manual ID, but without
	-- better controls on what runs when we can't solve this issue.
	elem:on_script_activated()

	return elem
end

function mission_manager:_tmp_room_pos(room_id, offset)
	local room = assert(self._world_def._roomlist._rooms[room_id], "Missing room " .. tostring(room_id))
	local centrex = room.x + room.w / 2
	local centrey = room.y + room.d / 2
	local cell_size = ProcedualMapGen.C.cell_size
	return Vector3(centrex * cell_size + (offset or 0) * 100, centrey * cell_size, 0)
end

mission_manager:init()

return mission_manager
