core:module("CoreElementRandom")
core:import("CoreMissionScriptElement")
core:import("CoreTable")

local LuaCallbackElement = class(CoreMissionScriptElement.MissionScriptElement)

function LuaCallbackElement:init(...)
	LuaCallbackElement.super.init(self, ...)
end

-- I don't think LuaCallbackElement:client_on_executed is needed?

function LuaCallbackElement:on_executed(instigator)
	LuaCallbackElement.super.on_executed(self, instigator)
	self._values.callback(self, instigator)
end

return LuaCallbackElement
