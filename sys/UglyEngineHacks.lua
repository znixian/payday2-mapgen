
-- If some other mod hasn't already set it up, create the IdString:s method
if not Idstring.s then
	-- Idstring:s returns the string used to build an Idstring. Not available on
	-- public DieselX builds, so use a string value (idstring::hex in C++)
	function Idstring:s()
		return self:key()
	end
end
