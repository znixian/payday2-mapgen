local custom_world_def = class()

local mapgen = blt.vm.dofile(ProcedualMapGen.ModPath .. "map/_init.lua")
local RoomGen = blt.vm.dofile(ProcedualMapGen.ModPath .. "geom/RoomGen.lua")
ProcedualMapGen.RoomGen = RoomGen

-- TODO require thingy for this, same as mapgen
local PathFinder = blt.vm.dofile(ProcedualMapGen.ModPath .. "path/PathFinder.lua")

local loads = {
-- These are all an attempt to get the player model working
"levels/narratives/elephant/chew/collision/collision",
"levels/narratives/bain/roberts/editor_only/editor_only",
"levels/instances/unique/are_loot_shoot/world/world",
"levels/narratives/bain/big/editor_only/editor_only",
"levels/instances/unique/dark/train_numbers/world/world",
"levels/instances/unique/pbr/pbr_flare/world/world",
"packages/sm_wish",
"packages/kosugi",
"packages/narr_hox_3",
"packages/narr_pal",
"packages/lvl_fish",
"packages/lvl_help",
"packages/narr_jerry2",
"levels/narratives/vlad/shout/world/world",
"levels/narratives/elephant/born/world/world",
"levels/narratives/vlad/peta/stage2/collision/collision",
"levels/narratives/elephant/born/collision/collision",
"levels/narratives/pbr/jerry/world/world",

-- This is requried for the debug floor (oneplanetorulethemall)
"levels/narratives/h_firestarter/stage_1/env/env",

-- Rex told me to use this for walls
"levels/narratives/e_welcome_to_the_jungle/stage_2/world/world",

-- Used for doors
"levels/narratives/short2/stage1/world",
"levels/narratives/short2/stage1/world/world",

-- Added for the loot props
"levels/narratives/bain/shadow_raid/world/world",
"levels/narratives/locke/brb/world/world",

-- Office props
"levels/narratives/dentist/hox/stage_2/world",
"levels/narratives/dentist/hox/stage_2/art/art",
}

function custom_world_def:init()
	managers.worlddefinition = self

	self._next_unit_id = 1000
	self._doors = {}
	self._units_by_id = {}

	-- same kind of stuff as in WorldHolder:create_world

	-- local path = ("levels/narratives/h_firestarter/stage_1/env/env")
	for _,path in ipairs(loads) do
		if not PackageManager:loaded(path) then
			PackageManager:load(path)
			-- table.insert(self._continent_packages, path)
		end
	end

	-- Set up the sequence manager
	-- This is important for all kinds of things, and if you don't run it then the player's arms don't appear
	-- Also, do this before we spawn any units. This is very important, since if we spawn a unit that references a
	-- nonexistant (in the SequenceManager eyes) sequence manager, it'll create a completely empty one. Haven't
	-- checked if that'll affect untis made after the sequence manager is loaded, but things like doors that we
	-- spawn will end up broken if we don't do this first.
	managers.sequence:preload()
	PackageManager:set_resource_loaded_clbk(Idstring("unit"), callback(managers.sequence, managers.sequence, "clbk_pkg_manager_unit_loaded"))

	-- Spawn in some basic geometry
	World:spawn_unit(Idstring("units/test/jocke/oneplanetorulethemall"), Vector3(0, 0, -100), Rotation())

	-- Set the seed for testing, so we can set the position of testing units
	-- TODO remove for prod
	math.randomseed(123456)

	local gen = mapgen:new()
	local rooms = gen:generate()

	-- Used by _tmp_room_pos in WorldDef
	self._roomlist = rooms

	-- error(json.encode(rooms))

	blt.xpcall(function()
		for _, room in ipairs(rooms._rooms) do
			local generator = room:pd2_generator() or RoomGen
			generator:generate(room, self)
		end
	end, function(msg)
		log("ERROR failed to generate world: " .. tostring(msg))
		log(debug.traceback())
	end)

	local pf = PathFinder:new(rooms)
	local navdata, cover_data = pf:build(rooms._rooms)

	self._cover_data = cover_data

	-- enable navman debug
	managers.navigation._use_fast_drawing = false

	managers.navigation._debug = true
	managers.navigation._nav_links = {}
	managers.navigation._pos_reservations = {}

	managers.navigation:clear()
	managers.navigation:set_load_data(navdata)
	log("set up navigation")

	-- Only set up group AI after setting the navigation data
	-- Otherwise it doesn't work
	managers.groupai:set_state("besiege") --(data.ai_settings.group_state)
	managers.ai_data:load_data(nil) -- (data.ai_data)

	managers.groupai:state():set_debug_draw_state(true)

	managers.navigation:set_debug_draw_state({
		doors = true,
		vis_graph = true,
		coarse_graph = true,
		-- blockers = true,

		-- boundaries = true,
		quads = true,
		nav_links = true,
		covers = true,
		pos_rsrv = true,
	})

	-- Setup the unit IDs
	-- These let us refer to units in elements
	for _, unit in ipairs(World:find_units_quick("all")) do
		local id = self._next_unit_id
		self._units_by_id[id] = unit
		self._next_unit_id = id + 1
		unit:unit_data().unit_id = id
	end
end

-- Used by CoreElementUnitSequence, since to start a sequence it uses a really roundabout way of doing it. This is because an element might be
-- loaded (and even called?) before the units it's attached to are spawned. So it spawns a dummy object and links a sequence on that dummy object
-- to the target object via this function, and then calls the sequence on the dummy object.
-- The normal implementation of this function handles the unit not being loaded, but since we spawn all our (environment) units before we spawn any
-- of our elements, that's not an issue.
function custom_world_def:add_trigger_sequence(unit, triggers)
	for _, trigger in ipairs(triggers) do
		local other = assert(self._units_by_id[trigger.notify_unit_id])
		unit:damage():add_trigger_sequence(trigger.name, trigger.notify_unit_sequence, other, trigger.time, nil, nil, is_editor)
	end
end

function custom_world_def:init_done()
	-- TODO
end

function custom_world_def:unload_packages()
	-- TODO
end

function custom_world_def:get_cover_data()
	return self._cover_data
end

-- Mod-specific functions

-- Register a door, when opened it enables the otherwise-disabled nav surface for [room]
function custom_world_def:register_door(unit, room)
	local door = {
		unit = unit,
		room = room,
	}
	table.insert(self._doors, door)
end

return custom_world_def
