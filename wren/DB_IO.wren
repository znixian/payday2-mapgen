import "base/native/LuaInterface_001" for LuaInterface
import "base/api/AssetUtil" for AssetUtil

class DBLoader {
    construct new() {
        _loaded = {}
    }

    add_bundle_fallback(ext, path) {
        AssetUtil.load_asset(path, ext)
    }
}

LuaInterface.register_object("DB_IO", DBLoader.new())
